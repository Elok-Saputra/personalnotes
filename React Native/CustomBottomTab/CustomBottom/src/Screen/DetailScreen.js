import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default function DetailScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text style={styles.marginBottom}>This is Detail Screen</Text>

      <TouchableOpacity
        onPress={() => navigation.push('Detail')}
        style={styles.marginBottom}>
        <Text>Go to Detail Screen...again</Text>
        {/* karena mau di load lg di stack */}
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.navigate('Home')}
        style={styles.marginBottom}>
        <Text>Go to Home</Text>
        {/* go  to home screen */}
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.marginBottom}>
        <Text>Go Back</Text>
        {/* go back to previous screen*/}
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.popToTop()}
        style={styles.marginBottom}>
        <Text>Go to the first screen</Text>
        {/* go  to whatever the first screen is*/}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  marginBottom: {
    marginBottom: 10,
  },
});
