import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default function FindScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text>This is Find Screen</Text>
      <TouchableOpacity onPress={() => navigation.navigate('Detail')}>
        <Text>Go to Detail Screen</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});
