import {StyleSheet} from 'react-native';

export const Color = StyleSheet.create({
  primary: {
    color: '#5e8b7e',
    backgroundColor: '#5e8b7e',
  },
  secondary: {
    color: '#a7c4bc',
    backgroundColor: '#a7c4bc',
  },
  third: {
    color: '#dfeeea',
    backgroundColor: '#dfeeea',
  },
  dark: {
    color: '#2f5d62',
    backgroundColor: '#2f5d62',
  },
});
