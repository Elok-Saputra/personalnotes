import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import HomeScreen from '../Screen/HomeScreen';
import FindScreen from '../Screen/FindScreen';
import ChatScreen from '../Screen/ChatScreen';
import PostScreen from '../Screen/PostScreen';
import SettingScreen from '../Screen/SettingScreen';

//icons
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({children, onPress}) => (
  <TouchableOpacity
    style={{
      top: -30,
      justifyContent: 'center',
      alignItems: 'center',
      ...styles.shadow,
    }}
    onPress={onPress}>
    <View style={styles.customButtonView}>{children}</View>
  </TouchableOpacity>
);

const Tabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: {
          position: 'absolute',
          bottom: 25,
          left: 20,
          right: 20,
          elevation: 0,
          backgroundColor: '#dfeeea',
          borderRadius: 15,
          height: 90,
          ...styles.shadow,
        },
      }}>
      {/* BottomTab Home */}
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.iconCentered}>
              <Entypo
                name="home"
                size={30}
                color="#a7c4bc"
                style={{color: focused ? '#5e8b7e' : '#a7c4bc'}}
              />
              <Text
                style={
                  (styles.textColor, {color: focused ? '#5e8b7e' : '#a7c4bc'})
                }>
                Home
              </Text>
            </View>
          ),
        }}
      />

      {/* BottomTab Find */}
      <Tab.Screen
        name="Find"
        component={FindScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.iconCentered}>
              <FontAwesome
                name="search"
                size={30}
                color="#a7c4bc"
                style={{color: focused ? '#5e8b7e' : '#a7c4bc'}}
              />
              <Text
                style={
                  (styles.textColor, {color: focused ? '#5e8b7e' : '#a7c4bc'})
                }>
                Find
              </Text>
            </View>
          ),
        }}
      />

      {/* BottomTab Post */}
      <Tab.Screen
        name="Post"
        component={PostScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <Entypo name="plus" size={60} style={{color: '#dfeeea'}} />
          ),
          tabBarButton: props => <CustomTabBarButton {...props} />,
        }}
      />

      {/* BottomTab Setting */}
      <Tab.Screen
        name="Setting"
        component={SettingScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.iconCentered}>
              <Ionicons
                name="ios-settings-sharp"
                size={30}
                color="#a7c4bc"
                style={{color: focused ? '#5e8b7e' : '#a7c4bc'}}
              />
              <Text
                style={
                  (styles.textColor, {color: focused ? '#5e8b7e' : '#a7c4bc'})
                }>
                Setting
              </Text>
            </View>
          ),
        }}
      />

      {/* BottomTab Chat */}
      <Tab.Screen
        name="Chat"
        component={ChatScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.iconCentered}>
              <Ionicons
                name="ios-chatbubbles-outline"
                size={30}
                color="#a7c4bc"
                style={{color: focused ? '#5e8b7e' : '#a7c4bc'}}
              />
              <Text
                style={
                  (styles.textColor, {color: focused ? '#5e8b7e' : '#a7c4bc'})
                }>
                Chat
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};
export default Tabs;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7f5df0',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
  textColor: {
    color: '#5e8b7e',
  },
  iconCentered: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  customButtonView: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#5e8b7e',
  },
});
