//======================================================================
//FUNCTION
//----------------------------------------------------------------------
//adalah kunci utama pada jacascript yang membuat javascript sangat powerfull --- Douglas Crockford ---
//----------------------------------------------------------------------

//function adalah sebuah sub-program/sub-routine yang dapat 'dipanggil' di bagian lain pada program

//merupakan struktur dasar pembentuk dari javascript

//disebut jg sebagai prosedur(kumpulan statement untuk melakuakn tugas atau menghitung sebuah nilai)

//untuk daoat menggunakannya, kita harus 'membuat' terlebih dahulu function tersebut, lalu 'memanggil' nya

//termasuk dalam first class object
//----------------------------------------------------------------------
//======================================================================

//======================================================================
//KENAPA FUNCTION?
//----------------------------------------------------------------------
//Reusability (DRY: Do not Repeat Yourself)
//daripada buat program berulang2 mending buat satu block function nanti tinggal dipanggil berulang2
//Dekomposisi/Abstraksi
//Modularitas
//----------------------------------------------------------------------
//======================================================================

//======================================================================
//KATEGORI FUNCTION
//----------------------------------------------------------------------
//Build-in function
//function yang sudah dibuatkan oleh javascript

//----------------------------------------------------------------------

//User-Defined Function
//function yang kita buat sendiri

//----------------------------------------------------------------------
//======================================================================

//======================================================================
//----------------------------------------------------------------------
//BUILD-IN FUNCTION
//----------------------------------------------------------------------
//contohnya: alert(), confirm(), prompt()
//dan masih banyak lainnya


//----------------------------------------------------------------------
//BUILD-IN FUNCTION: String
//----------------------------------------------------------------------
//contohnya: 
//charAt() utk mengetahui karakter apa yang ada dalam sebuah string
// let nama = 'Elok Marta Saputra'
// console.log(nama.charAt(6)) //akan menampilkan huruf a

//slice()
// console.log(nama.slice(3, 7))
//split(), toString(), toLowerCase(), toUpperCase(), ...

//----------------------------------------------------------------------
//BUILD-IN FUNCTION: Math
//----------------------------------------------------------------------
//contohnya:
//sin(), cos(), tan(), random(), round(), floor(), ceil(), log(), ...
// function getRandomInt(max) {
//     return Math.floor(Math.random() * max);
// }

// console.log(getRandomInt(5));
// // expected output: 0, 1, 2, 3, or 4

// console.log(getRandomInt(1));
// // expected output: 0

// console.log(Math.random());
//   // expected output: a number from 0 to <1

//======================================================================
//----------------------------------------------------------------------
//USER-DEFINED FUNCTION
//----------------------------------------------------------------------
//fungsi yang kita buat sendiri
//menggunakan keyword 'function'
//nama Function (opsional)
//bisa diberikan parameter atau argument disimpan dalam (), boleh ada boleh tidak dan boleh lebih dari satu, dipisahkan oleh koma (,)
//Function body 'dibungkus' dengan {}
//dapat mengembalikan nilai (return value) atau tidak

//----------------------------------------------------------------------
//MEMBUAT USER-DEFINED FUNCTION
//----------------------------------------------------------------------
//dengan Deklarasi / function Declaration
//dengan Ekspresi / Function Expression
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//Fuction Declaration
//----------------------------------------------------------------------
// function jumlahDuaBilangan(a, b) {
//     let total;
//     total = a + b

//     return total
// }
//ada keyword 'function' trus nama function 'jumlahDuaBilangan' trus parameter (a, b)

//----------------------------------------------------------------------
//Fuction Expression
//----------------------------------------------------------------------
// let jumlahDuaBilangan = function (a, b) {
//     let total;
//     total = a + b

//     return total
// }
//function disimpan ke dalam variable let trus ada nama function

//Memanggil / menjalankan function
// console.log(jumlahDuaBilangan(3, 5)) //function bisa di panggil berkali-kali
// console.log(jumlahDuaBilangan(10, 50))
// console.log(jumlahDuaBilangan(200.2, 50))

//======================================================================


//======================================================================
//----------------------------------------------------------------------
//CARA KERJA FUNCTION
//----------------------------------------------------------------------

//--INPUT-------------------------> FUNCTION -------------------> OUTPUT
//masukkan bahan/material        lakukan sesuatu thd           menghasilkan sesuatu
//kedalam function               bahan/material td                     

//----------------------------------------------------------------------
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Function yang baik hanya mengerjakan 1 hal saja
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//STUDY KASUS
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//menjumlahkan volue 2 kubus
//----------------------------------------------------------------------
//1. mengetahui sisi masing2 kubus
//   - kubus a sisinya 8
//   - kubus b sisinya 3
//2. hitung volume masing2 kubus
//   - sisi x sisi x sisi
//   - volume kubus a => 8^3 = 512
//   - volume kubus b => 3^3 = 27
//3. jumlahkan hasilnya
//   - 521 + 27
//4. kembalikan nilai jawabannya
//   - hasilnya 539

// const volumeKubus = function (a, b) {
//     let volumeA = a * a * a
//     let volumeB = b * b * b

//     let hasil = volumeA + volumeB
//     return hasil
// }
// console.log(volumeKubus(8, 3))
// console.log(volumeKubus(10, 15))

//----------------------------------------------------------------------
//======================================================================


//======================================================================
//----------------------------------------------------------------------
//PARAMETER DAN ARGUMEN
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//PARAMETER
//adalah variabel yg ditulis di dalam kurung pada saat function dibuat, digunakan utk menampung nilai yang dikirimkan saat function dipanggil
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//ARGUMENT
//adalah nilai yang dikirimkan ke parameter saat fungsi dipanggil
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//Contoh

// function tambah(a, b) {
//     return a + b
// }
// let res = tambah(2, 3) //bisa ditulis gini
// console.log(res)
// console.log(tambah(2, 3)) //atau bisa juga ditulis gini

//----------------------------------------------------------------------
//ARGUMENTS
//adalah array yang berisi nilai yang dikirimkan saat fungsi dipanggil
//----------------------------------------------------------------------

// function tambah() {
//     return arguments
// }
// let coba = tambah(5, 10, 20, 'hai', false)
// console.log(coba, 'arguments')

// function tambah1() {
//     let res = 0
//     for (let i = 0; i < arguments.length; i++) {
//         res += arguments[i]
//     }
//     return res
// }
// let cobacoba = tambah1(1, 2, 3) //menambahkan semua isi array yaitu 1+2+3 = 6
// console.log(cobacoba)
// let coba2 = tambah1(1, 2, 3, 4, 5) //menambahkan semua isi array yaitu 1+2+3+4+5 = 15
// console.log(coba2)

//----------------------------------------------------------------------
//======================================================================


//======================================================================
//----------------------------------------------------------------------
//REFACTORING
//adalah sebuah proses mengubah kode agar menjadi lebih 'baik' tanpa mengubah fungsionalitasnya
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//Kenapa REFACTORING?
// - Readability: kemudahan dalam membaca kode program
// - DRY (Don't Repeat Yourself)
// - Testability: penulisan kode agar mudah saat nantinya akan dilakukan pengujian
// - Performance: dapat meningkatkan performance dari program yang kita buat
// - Maintainability: bagaimana program nantinya dapat dengan mudah dikelola atau dikembangkan
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//contoh

// const volumeKubus = function (a, b) {
//     // let volumeA = a * a * a
//     // let volumeB = b * b * b

//     // let hasil = volumeA + volumeB
//     return a * a * a + b * b * b
// }
// console.log(volumeKubus(8, 3))
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//======================================================================


//======================================================================
//----------------------------------------------------------------------
//VARIABLE SCOPE
//----------------------------------------------------------------------
//adalah bagaimana sebuah variabel dapat diakses dalam program

//----------------------------------------------------------------------
//BLOCK SCOPE VS FUNCTION SCOPE
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//BLOCK SCOPE
//variable yg dibuat di dalam block scope (didalam {} suatu function) hanya berlaku di dalam block tersebut
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//TAPI di dalam javascript tidak mengenal block scope, tapi FUNCTION SCOPE
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//contoh

//global scope atau window scope yang bisa dipake dimanapun dalam program kita
// let a = 1

// function mobil() {
//     let b = 2
//     console.log(b) //kalo di dalam sini gak akan ada masalah
//     console.log(a) //ini bisa, karena variable a itu variable global

//     //ibaratnya kita ada dalam mobil yg pake kaca film yg gelap banget, nah kita bisa ngeliat keluar atau apa yang ada diluar mobil, tp yang diluar mobil gabisa ngeliat kedalam mobil atau susah ngeliat ke dalam mobil.
// }
// mobil()
// console.log(b) //ini akan error karena variable b adanya dalam function scopenya mobil()

//----------------------------------------------------------------------
//======================================================================

//======================================================================
//----------------------------------------------------------------------
//REKURSIF
//adalah sebuah fungsi yang memanggil dirinya sendiri
//----------------------------------------------------------------------
//======================================================================

// const tampilAngka = (n) => {
//     console.log(n)
//     return tampilAngka(n-1)
// }
// tampilAngka(10)

//ini kalo di console, hasilnya itung mundur dari 10 sampe minus (-) sekian, bakalan berenti lama banget.

//ketika pemanggilan fungsi Rekursif harus ada kapan harus berhentinya, disebut dengan

//Base Case
//adalah kondisi akhir dari rekursif yang menghasilkan nilai

// const tampilAngka = (n) => {
//     if (n === 0) {  //
//         return      // ini yang namanya Base case
//     }               // jika sudah sampai 0 maka gak akan ngereturn apapun
//     console.log(n)
//     return tampilAngka(n - 1)
// }
// tampilAngka(10)

//contoh case, menghitung Bilangan Faktorial
// const faktorial = (n) => {          
//     if (n === 0) return 1           

//     return n * faktorial(n - 1)     
// }                                   
// console.log(faktorial(5))           
//5 * faktorial(4)                      
//5 * (4 * faktorial(3))
//5 * (4 * (3 *faktorial(2)))
//5 * (4 * (3 * (2 * faktorial(1)))
//5 * (4 * (3 * (2 * (1)))
//5 * (4 * (3 * 2))
//5 * (4 * 6)
//5 * 24 = 120

//Rekursif
//semua looping bisa dibuatkan rekursifnya, tapi tidak sebaliknya

//ini loopingnya
// const tampilAngka = (n) => {
//     for(let i = n; i >= 1; i--) {
//         console.log(i)
//     }

// }

//ini rekursifnya
// const tampilAngka = (n) => {
//     if (n === 0) return      
//     console.log(n)
//     return tampilAngka(n - 1)
// }

//utk case Faktorial

//ini loopingnya
// const faktorial = (n) => {
//     let hasil = 1
//     for (let i = n; i > 0; i--) {
//         hasil *=  i
//     }
//     return hasil
// }

//ini rekursifnya
// const faktorial = (n) => {          
//     if (n === 0) return 1           

//     return n * faktorial(n - 1)     
// } 

//----------------------------------------------------------------------
//Implementasi Rekursif
// - menggantikan looping
// - Fibonacci
// - Pencarian dan penulusuran pada struktur data list dan tree
// - Untuk bahasa pemrograman yang tidak memiliki pengulangan

//----------------------------------------------------------------------
//======================================================================

//======================================================================
//----------------------------------------------------------------------
//FUNCTION: DECLARATION VS EXPRESSION
//----------------------------------------------------------------------
//======================================================================

//FUNCTION DECLARATION
//----------------------------------------------------------------------

//rumusnya
//function identifier (ParameterList opt) { Function Body}

// function tampilPesan (nama) {
//     alert('halo' + nama)
// }


//FUNCTION EXPRESSION
//Biasanya di simpan ke dalam sebuah variable
//----------------------------------------------------------------------

//rumusnya
//function identifier opt (ParameterList opt) { Function Body}

// let tampilPesan = function(nama) {
//     alert('halo' + nama)
// }

