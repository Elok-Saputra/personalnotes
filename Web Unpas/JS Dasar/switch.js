// let angka = prompt('masukkan angka: ')
//kalo pake prompt semua input yang dimasukin dianggap string jd operatornya pake (==) aja, tp kalo (===) depannya tambahin parseInt supaya inputan di convert ke integer
// let angka = parseInt(prompt('masukkan angka: '))

// switch (angka) {
//     case '1':
//         alert('anda memasukkan angka 1')
//         break
//     case '2':
//         alert('anda memasukkan angka 2')
//         break
//     case '3':
//         alert('anda memasukkan angka 3')
//         break
//     case '4':
//         alert('anda memasukkan angka 4')
//         break
//     default:
//         alert('angka yang anda masukkan salah')
//         break
// }

let item = prompt('masukkan nama makanan/minuman: \n (cth: nasi, daging, susu, hamberger, softdrink)')

switch (item) {
    case 'nasi':
        alert('makanan/minuman SEHAT')
        break
    case 'daging':
        alert('makanan/minuman SEHAT')
        break
    case 'susu':
        alert('makanan/minuman SEHAT')
        break
    case 'hamberger':
        alert('makanan/minuman TIDAK SEHAT')
        break
    case 'softdrink':
        alert('makanan/minuman TIDAK SEHAT')
        break
    default:
        alert('anda memasukkan makanan/minuman yang salah')
        break
}