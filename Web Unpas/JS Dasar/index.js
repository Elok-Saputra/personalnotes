const segitigaAnu = (e) => {
  let bentuk = "";
  for (let y = 0; y < e * 2 - 1; y++) {
    let w = y < e ? y : e * 2 - y - 2;
    bentuk += Array(e - w).join(" ") + Array(w + 1).join("* ") + "*\n";
  }
  console.log(bentuk);
};
console.log("--- segitiga ---");
segitigaAnu(7);

const primaAnu = (e) => {
  function isPrime(num) {
    for (let i = 2; i < num; i++) {
      if (num % i === 0) {
        return false;
      }
    }
    return true;
  }
  let arr = [2];
  for (let i = 3; i < e; i += 2) {
    if (isPrime(i)) {
      arr.push(i);
    }
  }
  console.log(...arr);
};
console.log("--- bilangan prima ---");
primaAnu(100);

const fibonaciAnu = (e) => {
  let n1 = 1,
    n2 = 1,
    nextTerm;
  let arr = [];
  for (let i = 1; i <= e; i++) {
    arr.push(n1);
    nextTerm = n1 + n2;
    n1 = n2;
    n2 = nextTerm;
  }
  console.log(...arr);
};
console.log("--- fibonaci ---");
fibonaciAnu(11);

const kelipatanAnu = (e) => {
  let arr = [];
  for (let i = 1; i <= 50; i++) {
    if (i % e == 0) arr.push(i);
  }
  console.log(...arr);
};
console.log("--- kelipatan ---");
kelipatanAnu(5);
