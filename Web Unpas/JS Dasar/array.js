//======================================================================
//----------------------------------------------------------------------
// ARRAY
//----------------------------------------------------------------------
//adalah tipe data yang digunakan untuk mendeskripsikan kumpulan elemen (nilai atau variable), yang tiap2 elemennya memiliki index

//----------------------------------------------------------------------
// KENAPA ARRAY?
// - untuk mempermudah pengelolaan nilai/value/data
//   + dalam penulusuran dan pencarian
// - Manajemen memori
//----------------------------------------------------------------------

//ARRAY
// - variable jamak, yang mempunyai banyak elemen dan diacu dengan nama yang sama
// - kumpulan pasangan key dan nilai / 'key and value pair'
// - key adalah index pada array dengan tipe integer yang dimulai dari 0
// - array pada javascript bertipe object
// - array padda jacascript memiliki fungsi / methode length untuk menghitung jumlah elemen di dalamnya
// - element pada array boleh memiliki tipe data yang berbeda

//KEY AND VALUE PAIR
//contoh:
// var binatang = ['Kucing', 'Kelinci', 'Monyet', 'Panda', 'Koala', 'Sapi']

//saat mengakses element
// console.log(binatang[4]) //yang tampil adalah Koala
// console.log(binatang.length) //yang tampil adalah 6

//element pada array boleh beda tipe
// let myArr = ['teks', 2, false]
// console.log(myArr)

//Multidimentional array adalah array di dalam array

//----------------------------------------------------------------------
//======================================================================


//======================================================================
//----------------------------------------------------------------------
// MANIPULASI ARRAY
//----------------------------------------------------------------------

// 1. Menambah isi array
// let arr = []
// arr[0] = 'Elok'
// arr[1] = 'Marta'
// arr[2] = 'Saputra'


// 2. Menghapus isi array
// let arr = ['Elok', 'Marta', 'Saputra']
// arr[1] = undefined

// 3. Menampilkan isi array
// let arr = ['Elok', 'Marta', 'Saputra']

// for (let index = 0; index < arr.length; index++) {
//     console.log(arr[index])
    
// }

// YANG DI ATAS MASIH PAKE CARA MANUAL

//ARRAY METHOD
//.length = mengetahui jumlah dari array

//.join = menggabungkan seluruh isi array dan mengubahnya menjadi srting
let arr = ['Elok', 'Marta', 'Saputra']
//secara default, separatornya pake koma (,)
console.log(arr.join(' - '))

//.push, pop, shift, unshift = menambah atau menghapus element dari array

//push (nambahin element array di belakang array)
//pop (ngehapus element array di belakang array)
console.log('------------ push and pop ---------------')
arr.push('mantap', 'anjing')
console.log(arr.join(' - '))

arr.pop()
console.log(arr.join(' - '))
arr.pop()
console.log(arr.join(' - '))
console.log('==============================================')
//unshift (nambahin element array di awal array)
//shift (ngehapus element array di awal array)
console.log('------------ unshift and shift ---------------')
console.log(arr.join(' - '))
arr.unshift('mister', "master")
console.log(arr.join(' - '))

arr.shift()
console.log(arr.join(' - '))
arr.shift()
console.log(arr.join(' - '))
console.log('==============================================')

//slice (mengambil beberapa bagian pada array buat dijadiin array yang baru)
//splice (menyambung atau menambal, menyisipkan element di tengah array)
console.log('------------ slice and splice ---------------')
console.log(arr.join(' - '))

//splice(indexAwal/mulaiDariIndexBerapa, mauDihapusBerapa, elementBaru1, elementBaru2,...)
arr.splice(2, 0, 'maronge', 'taliwang')
console.log(arr.join(' - '))
arr.splice(2, 2)
console.log(arr.join(' - '))

arr.splice(2, 0, 'maronge', 'taliwang')
console.log(arr.join(' - '))
//slice(awal, akhir)
let arr2 = arr.slice(2,4)
console.log(arr2.join(' - '))
